function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

console.log('Hello!');
await sleep(1000);
console.log('World!');

