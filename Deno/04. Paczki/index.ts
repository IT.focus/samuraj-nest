import * as base64 from "https://denopkg.com/chiefbiiko/base64/mod.ts";

const b64: string = base64.fromUint8Array(new TextEncoder().encode("this is too much"));
const buf: Uint8Array = base64.toUint8Array(b64);

console.log(b64);