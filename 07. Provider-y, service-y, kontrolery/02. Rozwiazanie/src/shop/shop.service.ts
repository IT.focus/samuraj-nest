import { Injectable } from '@nestjs/common';
import { ShopItem } from '../interfaces/shop';

@Injectable()
export class ShopService {
  getItems(): ShopItem[] {
    return [
      {
        name: 'Banan Afrykański',
        description: 'Super banan!',
        price: 5,
      },
      {
        name: 'Banan Europejski',
        description: 'To takie istnieją?',
        price: 4,
      },
      {
        name: 'Banan Zwyczajny',
        description: 'Niby zwyczajny, ale smaczny.',
        price: 4.5,
      },
    ];
  }

  hasItem(name: string): boolean {
    return this.getItems().some(item => item.name === name);
  }

  getPrice(name: string): number {
    return this.getItems().find(item => item.name === name).price;
  }
}
