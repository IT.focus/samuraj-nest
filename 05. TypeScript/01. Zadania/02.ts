const kitties = [
    {
        name: 'Mruczek',
        gender: 'male',
        age: 3,
        isAdopted: true,
        specialNeeds: ['Drinks only water'],
    },
    {
        name: 'Simon',
        gender: 'male',
        age: 'Unknown',
        isAdopted: false,
    },
    {
        name: 'Łatka',
        gender: 'female',
        age: 4,
        isAdopted: true,
    },
];
