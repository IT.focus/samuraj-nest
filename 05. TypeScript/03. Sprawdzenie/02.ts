kitties.push({
    name: 'Mila',
    age: '2',
    gender: 'Female',
    specialNeeds: [false], // No special needs
});

for (const kitty of kitties) {
    console.log('Kitty', kitty.name, kitty.surname);
    console.log('Year of birth', (new Date()).getFullYear() - kitty.age);
    if (kitty.gender === 'Female') {
        console.log('Is Female');
    } else {
        console.log('Is Male');
    }
    if (kitty.isadopted) {
        console.log('Is already adopted <3');
    }
}