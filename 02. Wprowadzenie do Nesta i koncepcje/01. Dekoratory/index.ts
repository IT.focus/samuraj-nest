import {countTime} from "./lib/count-time.decorator";

class Test {
    @countTime()
    someMethod() {
        let num = 2;
        for (let i = 1; i < 10000; i++) {
            num *= i;
        }
        console.log('Hello, World!', num);
    }
}

const test = new Test();
test.someMethod();