// Imperatywne

// const basket = [];
//
// basket.push({
//     count: 1,
//     price: 2.5,
//     name: 'bread',
// });
//
// basket.push({
//     count: 5,
//     price: 4,
//     name: 'cucumber',
// });
//
// let sum = 0;
// const names = [];
//
// for (let i = 0; i < basket.length; i++) {
//     sum += basket[i].count * basket[i].price;
//     names.push(basket[i].name);
// }
//
// console.log(names);
// console.log('Suma', sum);

// Obiektowe

class Basket {
    constructor() {
        this.items = [];
    }

    add(name, price, count = 1) {
        this.items.push({
            name,
            price,
            count,
        });
    }

    sum() {
        return this.items.reduce((prev, curr) => prev + curr.price * curr.count, 0);
    }

    names() {
        return this.items.map(item => item.name);
    }
}

const basket = new Basket();
basket.add('bread', 2.5);
basket.add('cucumber', 4, 5);

console.log(basket.names());
console.log('Suma', basket.sum());