function addProduct(currentBasket, name, price, count = 1) {
    return [...currentBasket, {
        name,
        price,
        count,
    }];
}

function itemNames(currentBasket) {
    return currentBasket.map(item => item.name);
}

function pricesSum(currentBasket) {
    return currentBasket.reduce((prev, curr) => prev + curr.price * curr.count, 0);
}

const basketWithBread = addProduct([], 'bread', 2.5);
const basketWithBreadAndCucumber = addProduct(basketWithBread, 'cucumber', 4, 5);

console.log(itemNames(basketWithBreadAndCucumber));
console.log('Suma', pricesSum(basketWithBreadAndCucumber));
