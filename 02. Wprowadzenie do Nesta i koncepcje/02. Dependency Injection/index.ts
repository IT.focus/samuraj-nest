import {HelloWorldModule} from "./modules/hello-world";
import {CalcModule} from "./modules/calc";
import {inject} from "./lib/inject.decorator";

class Test {
    @inject(HelloWorldModule, CalcModule)
    someMethod(
        helloWorld?: HelloWorldModule,
        calc?: CalcModule,
    ) {
        helloWorld.hello();
        console.log(calc.sum(2, 3));
    }
}

const test = new Test();
test.someMethod();

