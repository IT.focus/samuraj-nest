export function inject(...classes: any[]): any {
    return function (target, propertyKey: string, descriptor: PropertyDescriptor) {
        const original = descriptor.value;
        if (typeof original === 'function') {
            descriptor.value = function (...args) {
                const objects = classes.map(Cls => new Cls());
                return original(...objects, ...args);
            }
        }
        return descriptor;
    }
}